package net.tncy.calvet6u;


import org.junit.*;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class PersonValidationTest {

    private static Validator validator;

    public PersonValidationTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void validateWelldefinedPerson() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1974, Calendar.DECEMBER, 3);
        Date birthDate = calendar.getTime();
        Person p = new Person("Xavier", "Roy", birthDate, "FR");
        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p);
        assertEquals(0, constraintViolations.size());
    }

    @Test
    public void validateUndefinedPerson() {
        Person p = new Person();
        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p);
        assertEquals(2, constraintViolations.size());
    }

    @Test
    public void validateAnonymous() {
        Person p = new Person("", "", null, "");
        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p);
        assertEquals(2, constraintViolations.size());
    }

    @Test
    public void validateAdult() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1974, Calendar.DECEMBER, 3);
        Date birthDate = calendar.getTime();
        Person p = new Person("Xavier", "Roy", birthDate, "FR");
        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p, AdultCheck.class);
        assertEquals(0, constraintViolations.size());
    }

    @Test
    public void validateChild() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2003, Calendar.SEPTEMBER, 10);
        Date birthDate = calendar.getTime();
        Person p = new Person("Sibylle", "Roy", birthDate, "FR");
        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p, AdultCheck.class);
        assertEquals(1, constraintViolations.size());
    }

}
