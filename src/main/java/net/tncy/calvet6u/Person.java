package net.tncy.calvet6u;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Calendar;
import java.util.Date;

public class Person {

    @NotNull
    @Size(min = 1, message = "Le prénom doit contenir au moins 1 caractère")
    private String firstName;
    @NotNull
    @Size(min = 1)
    private String lastName;
    @NotNull(groups = {AdultCheck.class})
    private Date birthDate;
    private String citizenship;
    @NotNull(groups = {AdultCheck.class})
    @Min(value = 18, groups = {AdultCheck.class})
    private transient Integer age;

    public Person() {
    }

    public Person(String firstName, String lastName) {
        this();
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Person(String firstName, String lastName, Date birthDate, String citizenship) {
        this(firstName, lastName);
        this.birthDate = birthDate;
        this.citizenship = citizenship;
        computeAge();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        computeAge();
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public Integer getAge() {
        return age;
    }

    private void computeAge() {
        if (birthDate != null) {
            Calendar calendar = Calendar.getInstance();
            int currentYear = calendar.get(Calendar.YEAR);
            calendar.setTime(birthDate);
            int birthYear = calendar.get(Calendar.YEAR);
            age = currentYear - birthYear;
        } else {
            age = null;
        }

    }

}
